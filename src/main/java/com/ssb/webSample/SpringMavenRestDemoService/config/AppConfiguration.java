package com.ssb.webSample.SpringMavenRestDemoService.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.ssb.webSample.SpringMavenRestDemoService.data.CompanyDAO;
import com.ssb.webSample.SpringMavenRestDemoService.data.CompanyDAOImplementation;

/*
 * @author Shardul.Bartwal
 *
 */

@Configuration
@EnableWebMvc
public class AppConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost:5432/dovando");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres");

		return dataSource;
	}

	@Bean
	public CompanyDAO getCompanyDAO() {
		return new CompanyDAOImplementation(getDataSource());
	}
}