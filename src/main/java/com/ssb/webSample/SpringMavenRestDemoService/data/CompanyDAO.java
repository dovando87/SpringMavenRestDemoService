package com.ssb.webSample.SpringMavenRestDemoService.data;

import java.util.List;

import com.ssb.webSample.SpringMavenRestDemoService.model.Company;

/*
 * @author Shardul.Bartwal
 *
 */

public interface CompanyDAO {

	public void saveOrUpdate(Company company);

	public void delete(String companyId);

	public Company get(String companyId);

	public List<Company> list();
}