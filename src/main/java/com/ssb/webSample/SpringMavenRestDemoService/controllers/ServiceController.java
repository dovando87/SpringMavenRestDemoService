package com.ssb.webSample.SpringMavenRestDemoService.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.View;

import com.ssb.webSample.SpringMavenRestDemoService.data.CompanyDAO;
import com.ssb.webSample.SpringMavenRestDemoService.model.Company;

/*
 * @author Shardul.Bartwal
 *
 */

/**
 * ServiceController will be serving for all rest-full web client request.
 */
@RestController
@RequestMapping("/company")
public class ServiceController {

	@Autowired
	private CompanyDAO companyDAO;

	@Autowired
	private View jsonView;

	private static final Logger logger_c = Logger.getLogger(ServiceController.class);

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Company> listAllMembers() {
		return companyDAO.list();
	}

}